http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html

mvn archetype:generate -DgroupId=com.frotz.hello -DartifactId=hello -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
cd hello
mvn clean
mvn package
java -cp target/hello-1.0-SNAPSHOT.jar com.frotz.hello.App